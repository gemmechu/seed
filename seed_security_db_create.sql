
    create table administrator (
       administratorid bigint not null auto_increment,
        administrator_name varchar(255),
        date datetime,
        primary key (administratorid)
    ) engine=InnoDB

    create table branch (
       branchid bigint not null auto_increment,
        region varchar(255),
        branch_name varchar(255),
        primary key (branchid)
    ) engine=InnoDB

    create table cleaning_history (
       cleaning_history_id datetime not null,
        cleaned_amount integer not null,
        uncleaned_amount integer not null,
        wasted_amount integer not null,
        processing_station_variety_variety_historyid bigint,
        primary key (cleaning_history_id)
    ) engine=InnoDB

    create table company (
       companyid bigint not null,
        company_name varchar(255),
        date datetime,
        primary key (companyid)
    ) engine=InnoDB

    create table crop (
       cropid bigint not null auto_increment,
        crop_name varchar(255),
        primary key (cropid)
    ) engine=InnoDB

    create table farm (
       farmid bigint not null,
        region varchar(255),
        farm_name varchar(255),
        branch_branchid bigint,
        primary key (farmid)
    ) engine=InnoDB

    create table farm_variety_history (
       farm_variety_historyid bigint not null auto_increment,
        current_amount integer not null,
        date datetime,
        recieved_amount integer not null,
        status integer,
        farm_farmid bigint,
        processing_station_processing_stationid bigint,
        variety_varietyid bigint,
        primary key (farm_variety_historyid)
    ) engine=InnoDB

    create table processing_station (
       processing_stationid bigint not null,
        region varchar(255),
        processing_station_name varchar(255),
        branch_branchid bigint,
        primary key (processing_stationid)
    ) engine=InnoDB

    create table processing_station_variety_history (
       processing_station_variety_historyid bigint not null auto_increment,
        amount integer not null,
        buyer integer,
        buyer_name varchar(255),
        date datetime,
        status integer,
        farm_farmid bigint,
        processing_station_processing_stationid bigint,
        variety_varietyid bigint,
        primary key (processing_station_variety_historyid)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        enabled integer not null,
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table variety (
       varietyid bigint not null auto_increment,
        date datetime,
        variety_name varchar(255),
        crop_cropid bigint,
        primary key (varietyid)
    ) engine=InnoDB

    create table variety_status (
       variety_historyid bigint not null auto_increment,
        cleaned integer not null,
        current integer not null,
        uncleaned integer not null,
        processing_station_processing_stationid bigint,
        variety_varietyid bigint,
        primary key (variety_historyid)
    ) engine=InnoDB

    create table branch_farms (
       branch_branchid bigint not null,
        farms_farmid bigint not null,
        primary key (branch_branchid, farms_farmid)
    ) engine=InnoDB

    create table branch_processing_stations (
       branch_branchid bigint not null,
        processing_stations_processing_stationid bigint not null,
        primary key (branch_branchid, processing_stations_processing_stationid)
    ) engine=InnoDB

    create table company_branchs (
       company_companyid bigint not null,
        branchs_branchid bigint not null,
        primary key (company_companyid, branchs_branchid)
    ) engine=InnoDB

    create table company_farms (
       company_companyid bigint not null,
        farms_farmid bigint not null,
        primary key (company_companyid, farms_farmid)
    ) engine=InnoDB

    create table company_processing_stations (
       company_companyid bigint not null,
        processing_stations_processing_stationid bigint not null,
        primary key (company_companyid, processing_stations_processing_stationid)
    ) engine=InnoDB

    create table farm_variety (
       farm_farmid bigint not null,
        variety_varietyid bigint not null,
        primary key (farm_farmid, variety_varietyid)
    ) engine=InnoDB

    create table processing_station_processing_station_variety (
       processing_station_processing_stationid bigint not null,
        processing_station_variety_variety_historyid bigint not null,
        primary key (processing_station_processing_stationid, processing_station_variety_variety_historyid)
    ) engine=InnoDB

    create table processing_station_processing_station_variety_history (
       processing_station_processing_stationid bigint not null,
        processing_station_variety_history_processing_station_variety_historyid bigint not null,
        primary key (processing_station_processing_stationid, processing_station_variety_history_processing_station_variety_historyid)
    ) engine=InnoDB

    create table processing_station_variety (
       processing_station_processing_stationid bigint not null,
        variety_varietyid bigint not null,
        primary key (processing_station_processing_stationid, variety_varietyid)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table branch_farms 
       add constraint UK_ppc6ekkbcbbqbbycvyrn5rhev unique (farms_farmid)

    alter table branch_processing_stations 
       add constraint UK_c7nwrpnkhmcm1cxw42ue10r04 unique (processing_stations_processing_stationid)

    alter table company_branchs 
       add constraint UK_gw7vkv9kiaip25y2fuiagrnd8 unique (branchs_branchid)

    alter table company_farms 
       add constraint UK_98hlvqbg2yvljg1hps9m59pv0 unique (farms_farmid)

    alter table company_processing_stations 
       add constraint UK_a127mruborirvq6nlnar3a172 unique (processing_stations_processing_stationid)

    alter table farm_variety 
       add constraint UK_5nyegskbfcxub5k7jbob08ek8 unique (variety_varietyid)

    alter table processing_station_processing_station_variety 
       add constraint UK_1o1787ipc3j1tcu9k90b66x0c unique (processing_station_variety_variety_historyid)

    alter table processing_station_processing_station_variety_history 
       add constraint UK_437d0a9uncuhlh4flsb9jqi88 unique (processing_station_variety_history_processing_station_variety_historyid)

    alter table processing_station_variety 
       add constraint UK_3xol6mmda78xuplv548yyl6sq unique (variety_varietyid)

    alter table cleaning_history 
       add constraint FKruj2ypqd8rwxjrj79c9nievun 
       foreign key (processing_station_variety_variety_historyid) 
       references variety_status (variety_historyid)

    alter table farm 
       add constraint FKionolsfogk7694dvyjl6lng6e 
       foreign key (branch_branchid) 
       references branch (branchid)

    alter table farm_variety_history 
       add constraint FKpn483fcfw2d22ra00dkxb7rnb 
       foreign key (farm_farmid) 
       references farm (farmid)

    alter table farm_variety_history 
       add constraint FKpcksosfbj3k03mop777r9kfh5 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table farm_variety_history 
       add constraint FK4bbtrd6waum7gx1pwqe6l0ybj 
       foreign key (variety_varietyid) 
       references variety (varietyid)

    alter table processing_station 
       add constraint FKq9db2cm1aa0upo5qx01dsjyxa 
       foreign key (branch_branchid) 
       references branch (branchid)

    alter table processing_station_variety_history 
       add constraint FKjsie82wjwjd0sbde8je02whb 
       foreign key (farm_farmid) 
       references farm (farmid)

    alter table processing_station_variety_history 
       add constraint FKmmu5llq0sxln0586b7xs06bch 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table processing_station_variety_history 
       add constraint FKeo3jqfx525jw1obvu0umy8p3i 
       foreign key (variety_varietyid) 
       references variety (varietyid)

    alter table variety 
       add constraint FK9jxgmiyjycqj3mrcx2ebda9vj 
       foreign key (crop_cropid) 
       references crop (cropid)

    alter table variety_status 
       add constraint FKps7yquh4h9ethty2eu3ig8p30 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table variety_status 
       add constraint FKf89gbm8joyaerxac8vhkx6blo 
       foreign key (variety_varietyid) 
       references variety (varietyid)

    alter table branch_farms 
       add constraint FKenv17008jft9mxkr4qckqr1n4 
       foreign key (farms_farmid) 
       references farm (farmid)

    alter table branch_farms 
       add constraint FK8hqmb368qtarjqwpp3hg7ktd2 
       foreign key (branch_branchid) 
       references branch (branchid)

    alter table branch_processing_stations 
       add constraint FKapmojgf8q9xxuv4bp1ky00bgb 
       foreign key (processing_stations_processing_stationid) 
       references processing_station (processing_stationid)

    alter table branch_processing_stations 
       add constraint FK66mue07qj464qtpygnhana74o 
       foreign key (branch_branchid) 
       references branch (branchid)

    alter table company_branchs 
       add constraint FK5diqmqbvt7rtb2carw5ei5g1d 
       foreign key (branchs_branchid) 
       references branch (branchid)

    alter table company_branchs 
       add constraint FKjb53p3xbrvpsk7jyon8x0ti19 
       foreign key (company_companyid) 
       references company (companyid)

    alter table company_farms 
       add constraint FKk3vwe8th32lsclmk9com3trje 
       foreign key (farms_farmid) 
       references farm (farmid)

    alter table company_farms 
       add constraint FKf20khrxcbdyys28q23apafj7g 
       foreign key (company_companyid) 
       references company (companyid)

    alter table company_processing_stations 
       add constraint FKkdo6wbjrjam67752sangw0pm9 
       foreign key (processing_stations_processing_stationid) 
       references processing_station (processing_stationid)

    alter table company_processing_stations 
       add constraint FKkjplmkrx2qmidv28175gi3i0v 
       foreign key (company_companyid) 
       references company (companyid)

    alter table farm_variety 
       add constraint FKljvucejp6t5hyfdg477vxpcxr 
       foreign key (variety_varietyid) 
       references variety (varietyid)

    alter table farm_variety 
       add constraint FKk6yq7h8c7y3p0tkd9p4dgf7pt 
       foreign key (farm_farmid) 
       references farm (farmid)

    alter table processing_station_processing_station_variety 
       add constraint FKi2f5lkdybax97f0gcabwt5bys 
       foreign key (processing_station_variety_variety_historyid) 
       references variety_status (variety_historyid)

    alter table processing_station_processing_station_variety 
       add constraint FKd75ijvpofndh0b12kvc01s5jd 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table processing_station_processing_station_variety_history 
       add constraint FKtgnc1r2ve2419ddoa8dst9rdv 
       foreign key (processing_station_variety_history_processing_station_variety_historyid) 
       references processing_station_variety_history (processing_station_variety_historyid)

    alter table processing_station_processing_station_variety_history 
       add constraint FKegjri2wq2safii6iyva89t0ih 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table processing_station_variety 
       add constraint FK8xttq1aed25tgifmxrktbn9aq 
       foreign key (variety_varietyid) 
       references variety (varietyid)

    alter table processing_station_variety 
       add constraint FKn2mwjfpa9w6bvwepgfji1wnbw 
       foreign key (processing_station_processing_stationid) 
       references processing_station (processing_stationid)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FK859n2jvi8ivhui0rl0esws6o 
       foreign key (user_id) 
       references user (id)
