package com.seed.components;

import com.seed.models.Company;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CompanyValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Company.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyName", "error.companyName", "Company Name is required.");
    }
}
