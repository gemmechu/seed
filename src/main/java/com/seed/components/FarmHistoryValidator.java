package com.seed.components;

import com.seed.models.FarmVarietyHistory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FarmHistoryValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return FarmVarietyHistory.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "farm", "error.farm", "Farm is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "variety", "error.variety", "Variety is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "recievedAmount", "error.recievedAmount", "Received Amount is required");
    }
}
