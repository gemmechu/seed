package com.seed.components;

import com.seed.models.CleaningHistory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CleaningHistoryValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return CleaningHistory.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"cleanedAmount", "errors.cleanedAmount","Cleaned Amount must be a valid value.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"uncleanedAmount", "errors.uncleanedAmount","Uncleaned Amount must be a valid value.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"wastedAmount", "errors.wastedAmount","Wasted Amount must be a valid value.");
    }
}
