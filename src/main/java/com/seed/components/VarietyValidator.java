package com.seed.components;

import com.seed.models.Variety;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class VarietyValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Variety.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "varietyName", "error.varietyName", "Variety Name must be valid.");
    }
}
