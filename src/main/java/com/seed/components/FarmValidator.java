package com.seed.components;

import com.seed.models.Farm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FarmValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Farm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "farmName", "error.farmName", "Farm Name is required.");

    }
}
