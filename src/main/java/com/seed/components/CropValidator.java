package com.seed.components;

import com.seed.models.Crop;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CropValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Crop.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cropName", "error.cropName", "Crop Name is required.");
    }
}
