package com.seed.components;

import com.seed.models.ProcessingStationVarietyHistory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProcessingStationVarietyHistoryValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return ProcessingStationVarietyHistory.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //Farm Variety PS Amount
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "farm", "error.farm", "Farm is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "processingStation", "error.processingStation", "Processing Station is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "variety", "error.variety", "Variety is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amount", "error.amount", "Amount is required.");
    }
}
