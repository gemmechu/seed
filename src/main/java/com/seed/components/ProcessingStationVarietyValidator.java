package com.seed.components;

import com.seed.models.ProcessingStationVarietyStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProcessingStationVarietyValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return ProcessingStationVarietyStatus.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //cl,uncl,currr, ps,var
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cleaned", "error.cleaned", "Cleaned Amount must be valid");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uncleaned", "error.uncleaned", "Uncleaned Amount must be valid");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "current", "error.current", "Current Amount must be valid");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "processingStation", "error.processingStation", "Processing Station must be valid");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "variety", "error.variety", "Variety must be valid");

    }
}
