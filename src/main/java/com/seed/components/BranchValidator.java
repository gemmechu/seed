package com.seed.components;

import com.seed.models.Branch;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BranchValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Branch.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "branchName", "error.branchName", "Branch Name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "region", "error.region", "Branch Region is required.");
    }
}
