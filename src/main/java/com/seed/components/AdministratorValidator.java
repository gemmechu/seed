package com.seed.components;

import com.seed.models.Administrator;
import com.seed.models.FarmVarietyHistory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AdministratorValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Administrator.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "administratorName", "error.administratorName", "Administrator Name is required");
    }
}
