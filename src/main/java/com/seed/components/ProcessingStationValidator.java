package com.seed.components;

import com.seed.models.ProcessingStation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProcessingStationValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return ProcessingStation.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "processingStationName", "error.processingStationName", "Processing Station Name is required.");

    }
}
