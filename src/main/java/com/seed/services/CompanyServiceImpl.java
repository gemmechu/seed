package com.seed.services;

import com.seed.models.Company;
import com.seed.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public void saveCompany(Company company) {
		companyRepository.save(company);
	}

	@Override
	public List<Company> findAll() {
		return companyRepository.findAll();
	}

	@Override
	public Optional<Company> findByCompanyID(Long companyID) {
		return companyRepository.findById(companyID);
	}

}

 