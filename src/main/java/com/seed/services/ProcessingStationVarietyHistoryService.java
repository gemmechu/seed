package com.seed.services;

import com.seed.models.ProcessingStationVarietyHistory;

import java.util.List;
import java.util.Optional;

public interface ProcessingStationVarietyHistoryService {
    void save(ProcessingStationVarietyHistory processingStationVarietyHistory);
    List<ProcessingStationVarietyHistory> findAll();
    Optional<ProcessingStationVarietyHistory> findByProcessingStationVarietyHistoryID(Long ProcessingStationVarietyHistoryID);

}
