package com.seed.services;

import com.seed.models.ProcessingStationVarietyHistory;
import com.seed.repositories.ProcessingStationVarietyHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProcessingStationVarietyHistoryServiceImpl implements ProcessingStationVarietyHistoryService {
    @Autowired
    ProcessingStationVarietyHistoryRepository processingStationVarietyHistoryRepository;
    @Override
    public void save(ProcessingStationVarietyHistory processingStationVarietyHistory) {
        processingStationVarietyHistoryRepository.save(processingStationVarietyHistory);
    }

    @Override
    public List<ProcessingStationVarietyHistory> findAll() {
        return processingStationVarietyHistoryRepository.findAll();
    }

    @Override
    public Optional<ProcessingStationVarietyHistory> findByProcessingStationVarietyHistoryID(Long ProcessingStationVarietyHistoryID) {
        return processingStationVarietyHistoryRepository.findById(ProcessingStationVarietyHistoryID);
    }


}
