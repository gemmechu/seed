package com.seed.services;

import com.seed.security.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.validation.Valid;

public interface UserService extends UserDetailsService{
	
	User findUserByUsername(String username);
	void saveUser(User user);
	void saveUser(User user, String role, String name);
	void saveBranch(User user, String role, String name, String region, Long id);
	void saveProcessingStation(@Valid User user, String seed_processing_station, String name, String region, String branch, Long id);
	void saveFarm(User user, String role, String name, String region, String branchName, Long id);
}
