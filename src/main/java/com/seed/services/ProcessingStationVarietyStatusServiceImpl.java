package com.seed.services;

import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyStatus;
import com.seed.models.Variety;
import com.seed.repositories.ProcessingStationVarietyStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProcessingStationVarietyStatusServiceImpl implements ProcessingStationVarietyStatusService{

	@Autowired
	private ProcessingStationVarietyStatusRepository processingStationVarietyStatusRepository;


	@Override
	public void saveProcessingStationVarietyStatus(ProcessingStationVarietyStatus processingStationVarietyStatus) {
		processingStationVarietyStatusRepository.save(processingStationVarietyStatus);
	}

	@Override
	public List<ProcessingStationVarietyStatus> findAll() {
		// TODO Auto-generated method stub
		return processingStationVarietyStatusRepository.findAll();
	}

	@Override
	public Optional<ProcessingStationVarietyStatus> findByProcessingStationVarietyStatusID(Long processingStationVarietyID) {
		return processingStationVarietyStatusRepository.findById(processingStationVarietyID);
	}



	@Override
	public Optional<ProcessingStationVarietyStatus> findByVarietyAndProcessingStation(Variety variety, ProcessingStation processingStation) {
		return processingStationVarietyStatusRepository.findByVarietyAndProcessingStation(variety,processingStation);
	}

	@Override
	public Set<ProcessingStationVarietyStatus> findByVariety(Variety variety) {
		return processingStationVarietyStatusRepository.findByVariety(variety);
	}

}
