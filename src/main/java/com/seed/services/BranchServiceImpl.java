package com.seed.services;

import com.seed.models.Branch;
import com.seed.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BranchServiceImpl implements BranchService {

	@Autowired
	private BranchRepository branchRepository;
	@Override
	public void saveBranch(Branch branch) {
		// TODO Auto-generated method stub
		branchRepository.save(branch);
		
	}

	@Override
	public List<Branch> findAll() {
		// TODO Auto-generated method stub
		return branchRepository.findAll();
	}

	@Override
	public Optional<Branch> findByBranchID(Long branchID) {
		// TODO Auto-generated method stub
		return branchRepository.findById(branchID);
	}

	@Override
	public Optional<Branch> findByBranchName(String branchName) {
		return branchRepository.findByBranchName(branchName);
	}

}
