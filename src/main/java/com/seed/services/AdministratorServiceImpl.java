package com.seed.services;

import com.seed.models.Administrator;
import com.seed.repositories.AdministratorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdministratorServiceImpl implements AdministratorService {

	@Autowired
	private AdministratorRepository adminRepository;

	@Override
	public void saveGovernment(Administrator administrator) {
		adminRepository.save(administrator);
	}

	@Override
	public List<Administrator> findAll() {
		return adminRepository.findAll();
	}

	@Override
	public Optional<Administrator> findByGovernmentID(Long governmentID) {
		return adminRepository.findById(governmentID);
	}

}
