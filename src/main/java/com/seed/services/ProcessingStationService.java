package com.seed.services;

import com.seed.models.ProcessingStation;

import java.util.List;
import java.util.Optional;

public interface ProcessingStationService {
    void saveProcessingStation(ProcessingStation processingStation);
    List<ProcessingStation> findAll();
    Optional<ProcessingStation> findByProcessingStationID(Long processingStationID);
    Optional<ProcessingStation> findByProcessingStationName(String processingStationName);
}
