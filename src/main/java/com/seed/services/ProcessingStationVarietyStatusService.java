package com.seed.services;

import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyStatus;
import com.seed.models.Variety;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProcessingStationVarietyStatusService {
	 void saveProcessingStationVarietyStatus(ProcessingStationVarietyStatus processingStationVarietyStatus);
	    List<ProcessingStationVarietyStatus> findAll();
	    Optional<ProcessingStationVarietyStatus> findByProcessingStationVarietyStatusID(Long processingStationVarietyStatusID);
	Optional<ProcessingStationVarietyStatus> findByVarietyAndProcessingStation(Variety variety, ProcessingStation processingStation);
	Set<ProcessingStationVarietyStatus> findByVariety(Variety variety);
}
