package com.seed.services;

import com.seed.models.Crop;
import com.seed.repositories.CropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CropServiceImpl implements CropService {

	@Autowired
    CropRepository cropRepository;
	@Override
	public void saveCrop(Crop Crop) {
		// TODO Auto-generated method stub
		cropRepository.save(Crop);
	}

	@Override
	public List<Crop> findAll() {
		// TODO Auto-generated method stub
		return cropRepository.findAll();
	}

	@Override
	public Optional<Crop> findByCropID(Long CropID) {
		// TODO Auto-generated method stub
		return  cropRepository.findById(CropID);
	}

	@Override
	public Optional<Crop> findByCropName(String CropName) {
		// TODO Auto-generated method stub
		return cropRepository.findByCropName(CropName);
	}

}
