package com.seed.services;

import com.seed.models.Farm;
import com.seed.repositories.FarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FarmServiceImpl implements FarmService {
	@Autowired
	private FarmRepository farmRepository;
	@Override
	public void saveFarm(Farm farm) {
		farmRepository.save(farm);
	}

	@Override
	public List<Farm> findAll() {
		return farmRepository.findAll();
	}

	@Override
	public Optional<Farm> findByFarmID(Long farmId) {
		return farmRepository.findById(farmId);
	}

	@Override
	public Optional<Farm> findByFarmName(String farmName) {
		return  farmRepository.findByFarmName(farmName);
	}


}
