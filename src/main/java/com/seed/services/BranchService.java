package com.seed.services;

import com.seed.models.Branch;

import java.util.List;
import java.util.Optional;

public interface BranchService {

	void saveBranch(Branch branch);
    List<Branch> findAll();
    Optional<Branch> findByBranchID(Long branchID);
    Optional<Branch> findByBranchName(String branchName);
}
