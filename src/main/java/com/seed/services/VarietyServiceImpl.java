package com.seed.services;

import java.util.List;
import java.util.Optional;

import com.seed.models.Crop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seed.models.Variety;
import com.seed.repositories.VarietyRepository;
@Service
public class VarietyServiceImpl implements VarietyService{
@Autowired
VarietyRepository varietyRepository; 
	@Override
	public void saveVariety(Variety Variety) {
		// TODO Auto-generated method stub
		varietyRepository.save(Variety);
		
	}

	@Override
	public List<Variety> findAll() {
		// TODO Auto-generated method stub
		return varietyRepository.findAll();
	}

	@Override
	public Optional<Variety> findByVarietyID(Long VarietyID) {
		// TODO Auto-generated method stub
		return varietyRepository.findById(VarietyID);
	}

	@Override
	public Optional<Variety> findByVarietyName(String VarietyName) {
		// TODO Auto-generated method stub
		return varietyRepository.findByVarietyName(VarietyName);
	}

	@Override
	public Optional<Variety> findByVarietyNameAndCrop(String VarietyName, Crop crop) {
		return varietyRepository.findByVarietyNameAndCrop(VarietyName,crop);
	}

}
