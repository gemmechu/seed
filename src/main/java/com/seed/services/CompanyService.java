package com.seed.services;

import com.seed.models.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyService {

    void saveCompany(Company company);
    List<Company> findAll();
    Optional<Company> findByCompanyID(Long companyID);
}
