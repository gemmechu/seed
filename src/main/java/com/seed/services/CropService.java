package com.seed.services;

import com.seed.models.Crop;

import java.util.List;
import java.util.Optional;

public interface CropService {
	void saveCrop(Crop Crop);
    List<Crop> findAll();
    Optional<Crop> findByCropID(Long CropID);
    Optional<Crop> findByCropName(String CropName);
}
