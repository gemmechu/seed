package com.seed.services;

import com.seed.models.ProcessingStation;
import com.seed.repositories.ProcessingStationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProcessingStationServiceImpl implements ProcessingStationService {

	@Autowired
	public ProcessingStationRepository processingStationRepository;

	@Override
	public void saveProcessingStation(ProcessingStation processingStation) {
		processingStationRepository.save(processingStation);
	}

	@Override
	public List<ProcessingStation> findAll() {
		return processingStationRepository.findAll();
	}

	@Override
	public Optional<ProcessingStation> findByProcessingStationID(Long processingStationID) {
		return processingStationRepository.findById(processingStationID);
	}

	@Override
	public Optional<ProcessingStation> findByProcessingStationName(String processingStationName) {
		return processingStationRepository.findByProcessingStationName(processingStationName);
	}

}
