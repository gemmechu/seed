package com.seed.services;

import java.util.List;
import java.util.Optional;

import com.seed.models.Crop;
import com.seed.models.Variety;

public interface VarietyService {

	void saveVariety(Variety Variety);
    List<Variety> findAll();
    Optional<Variety> findByVarietyID(Long VarietyID);
    Optional<Variety> findByVarietyName(String VarietyName);
    Optional<Variety> findByVarietyNameAndCrop(String VarietyName, Crop crop);
}
