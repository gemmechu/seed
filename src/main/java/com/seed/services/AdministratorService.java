package com.seed.services;

import com.seed.models.Administrator;

import java.util.List;
import java.util.Optional;

public interface AdministratorService {
    void saveGovernment(Administrator administrator);
    List<Administrator> findAll();
    Optional<Administrator> findByGovernmentID(Long governmentID);
}
