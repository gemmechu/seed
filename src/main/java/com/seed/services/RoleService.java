package com.seed.services;

import com.seed.security.Role;

import java.util.Optional;

public interface RoleService {

	public Role save(Role role);

	Optional<Role> findById(Long id);

	Optional<Role> findByRole(String role);
	 
	Iterable<Role> findAll();
	
	void deleteById(Long id);
	
	void delete(Role role);
}
