package com.seed.services;

import com.seed.models.Farm;
import com.seed.models.FarmVarietyHistory;
import com.seed.models.Variety;

import java.util.List;
import java.util.Optional;

public interface FarmVarietyHistoryService {
    boolean saveFarmVarietyHistoryService(FarmVarietyHistory farmVarietyHistory);
    List<FarmVarietyHistory> findAll();
    Optional<FarmVarietyHistory> findByFarmVarietyHistoryId(Long farmVarietyHistoryId);
    FarmVarietyHistory findByVarietyAndFarm(Variety variety, Farm farm);

}
