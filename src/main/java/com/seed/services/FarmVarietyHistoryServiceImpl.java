package com.seed.services;

import com.seed.models.Farm;
import com.seed.models.FarmVarietyHistory;
import com.seed.models.ProcessingStation;
import com.seed.models.Variety;
import com.seed.repositories.FarmRepository;
import com.seed.repositories.FarmVarietyHistoryRepository;
import com.seed.repositories.ProcessingStationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PatchMapping;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class FarmVarietyHistoryServiceImpl implements FarmVarietyHistoryService {
    @Autowired
    private FarmVarietyHistoryRepository farmVarietyHistoryRepository;
    @Autowired
    private FarmRepository farmRepository;
    @Autowired
    private ProcessingStationRepository processingStationRepository;

    @PatchMapping
    @Override
    public boolean saveFarmVarietyHistoryService(FarmVarietyHistory farmVarietyHistory) {
        if(farmVarietyHistory.getStatus()== FarmVarietyHistory.Status.DISPATCHED){
            FarmVarietyHistory farmVarietyHistory1 = findByVarietyAndFarm(farmVarietyHistory.getVariety(), farmVarietyHistory.getFarm());
            if(farmVarietyHistory1==null || farmVarietyHistory1.getCurrentAmount()<farmVarietyHistory.getRecievedAmount()){
                return false;
            }
            else {
                farmVarietyHistory.setCurrentAmount(farmVarietyHistory1.getCurrentAmount() - farmVarietyHistory.getRecievedAmount());
            }
            ProcessingStation processingStation = farmVarietyHistory.getProcessingStation();
            Variety variety = farmVarietyHistory.getVariety();
            Set<Variety> varietySet = processingStation.getVariety();
            if(!varietySet.contains(variety)) {
                varietySet.add(variety);
                Optional<ProcessingStation> processingStation1 = processingStationRepository.findById(processingStation.getProcessingStationID());
                if (processingStation1.isPresent()) {
                    processingStation1.get().setVariety(varietySet);
                    processingStationRepository.save(processingStation1.get());
                }
            }

            farmVarietyHistoryRepository.save(farmVarietyHistory);
            return true;
        }
        else if(farmVarietyHistory.getStatus()== FarmVarietyHistory.Status.RECEIVED){
            FarmVarietyHistory farmVarietyHistory1 = findByVarietyAndFarm(farmVarietyHistory.getVariety(), farmVarietyHistory.getFarm());
            if(farmVarietyHistory1==null){
                farmVarietyHistory.setCurrentAmount(farmVarietyHistory.getRecievedAmount());
            }
            else{
                farmVarietyHistory.setCurrentAmount(farmVarietyHistory1.getCurrentAmount()+farmVarietyHistory.getRecievedAmount());
            }
            Farm farm = farmVarietyHistory.getFarm();
            Variety variety = farmVarietyHistory.getVariety();
            Set<Variety> varietySet = farm.getVariety();
            if(!varietySet.contains(variety)) {
                varietySet.add(variety);
                Optional<Farm> farm1 = farmRepository.findById(farm.getFarmID());
                if (farm1.isPresent()) {
                    farm1.get().setVariety(varietySet);
                    farmRepository.save(farm1.get());
                }
            }
            farmVarietyHistoryRepository.save(farmVarietyHistory);
            return true;
        }
        return false;
    }

    @Override
    public List<FarmVarietyHistory> findAll() {
        return farmVarietyHistoryRepository.findAll();
    }

    @Override
    public Optional<FarmVarietyHistory> findByFarmVarietyHistoryId(Long farmVarietyHistoryId) {
        return farmVarietyHistoryRepository.findById(farmVarietyHistoryId);
    }

    @Override
    public FarmVarietyHistory findByVarietyAndFarm(Variety variety, Farm farm) {
        List<FarmVarietyHistory> farmVarietyHistories = farmVarietyHistoryRepository.findByVarietyAndFarm(variety, farm);
        if(farmVarietyHistories.size()==0){
            return null;
        }
        return farmVarietyHistories.get(farmVarietyHistories.size()-1);
    }
}
