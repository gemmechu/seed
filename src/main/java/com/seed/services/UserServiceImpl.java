package com.seed.services;

import com.seed.models.Branch;
import com.seed.models.Company;
import com.seed.models.Farm;
import com.seed.models.ProcessingStation;
import com.seed.repositories.*;
import com.seed.security.Role;
import com.seed.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PatchMapping;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@Service
public class UserServiceImpl implements UserService {
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private ProcessingStationRepository processingStationRepository;
	private FarmRepository farmRepository;
	private BranchRepository branchRepository;
	private CompanyRepository companyRepository;
	private AdministratorRepository administratorRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, FarmRepository farmRepository, ProcessingStationRepository processingStationRepository, BranchRepository branchRepository, CompanyRepository companyRepository, AdministratorRepository administratorRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.processingStationRepository = processingStationRepository;
		this.farmRepository=farmRepository;
		this.companyRepository = companyRepository;
		this.administratorRepository = administratorRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.branchRepository=branchRepository;
	}
	public User findUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	@Override
	public void saveUser(User user, String role, String name) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEnabled(1);
		Optional<Role> userRole = roleRepository.findByRole(role);
		if(userRole.isPresent()) {
			Set<Role> roleSet = new HashSet<Role>();
			roleSet.add(userRole.get());
			user.setRoles(roleSet);
			userRepository.save(user);
			Company company = new Company();
			company.setCompanyID(user.getId());
			company.setCompanyName(name);
			companyRepository.save(company);
		}
	}

	@PatchMapping
	@Override
	public void saveProcessingStation(User user, String role, String name, String region, String branchName, Long id) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEnabled(1);
		Optional<Role> userRole = roleRepository.findByRole(role);
		if(userRole.isPresent()) {
			Set<Role> roleSet = new HashSet<Role>();
			roleSet.add(userRole.get());
			user.setRoles(roleSet);
			userRepository.save(user);
			ProcessingStation processingStation = new ProcessingStation();
			Optional<Company> company = companyRepository.findById(id);
			Optional<Branch> branch = branchRepository.findByBranchName(branchName);
			company.ifPresent(company1 -> {
				processingStation.setProcessingStationID(user.getId());
				processingStation.setRegion(region);
				processingStation.setProcessingStationName(name);
				processingStation.setProcessingStationID(user.getId());
				if(branch.isPresent()) {
					Branch branch1= branch.get();
					processingStation.setBranch(branch1);
				}

				processingStationRepository.save(processingStation);
				Set<ProcessingStation> processingStationSet = company1.getProcessingStations();
				processingStationSet.add(processingStation);
				company1.setProcessingStations(processingStationSet);
				companyRepository.save(company1);
				branch.ifPresent(branch1 -> {
					Set<ProcessingStation> processingStationSet1 = branch1.getProcessingStations();
					processingStationSet1.add(processingStation);
					branch1.setProcessingStations(processingStationSet1);
					branchRepository.save(branch1);
				});
			});
		}
	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if(user != null) {
			return user;
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
	}
	@PatchMapping
	@Override
	public void saveBranch(User user, String role, String name, String region, Long id) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEnabled(1);
		Optional<Role> userRole = roleRepository.findByRole(role);
		if(userRole.isPresent()) {
			Set<Role> roleSet = new HashSet<Role>();
			roleSet.add(userRole.get());
			user.setRoles(roleSet);
			userRepository.save(user);
			Branch branch = new Branch();
			Optional<Company> company2 = companyRepository.findById(id);
			company2.ifPresent(company -> {
				branch.setRegion(region);
				branch.setBranchName(name);
				branchRepository.save(branch);
				Set<Branch> branchSet = company.getBranchs();
				branchSet.add(branch);
				company.setBranchs(branchSet);
				companyRepository.save(company);
			});
		}

	}
	@PatchMapping
	@Override
	public void saveFarm(User user, String role, String name, String region, String branchName, Long id) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEnabled(1);
		Optional<Role> userRole = roleRepository.findByRole(role);
		if(userRole.isPresent()) {
			Set<Role> roleSet = new HashSet<Role>();
			roleSet.add(userRole.get());
			user.setRoles(roleSet);
			userRepository.save(user);
			Farm farm = new Farm();
			Optional<Company> company = companyRepository.findById(id);
			Optional<Branch> branch = branchRepository.findByBranchName(branchName);
			company.ifPresent(company1 -> {
				farm.setRegion(region);
				farm.setFarmName(name);
				farm.setFarmID(user.getId());
				if(branch.isPresent()) {
					Branch branch1= branch.get();
					farm.setBranch(branch1);
				}
				farmRepository.save(farm);
				Set<Farm> farmSet = company1.getFarms();
				farmSet.add(farm);
				company1.setFarms(farmSet);
				companyRepository.save(company1);
				branch.ifPresent(branch1 -> {
					Set<Farm> farmSet1 = branch1.getFarms();
					farmSet1.add(farm);
					branch1.setFarms(farmSet1);
					branchRepository.save(branch1);
				});
			});
		}
	}

}
