package com.seed.services;

import com.seed.models.Farm;

import java.util.List;
import java.util.Optional;


public interface FarmService {
	void saveFarm(Farm farm);
    List<Farm> findAll();
    Optional<Farm> findByFarmID(Long FarmID);
    Optional<Farm> findByFarmName(String farmName);

}
