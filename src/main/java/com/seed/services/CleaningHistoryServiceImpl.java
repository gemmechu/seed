package com.seed.services;

import com.seed.models.CleaningHistory;
import com.seed.repositories.CleaningHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CleaningHistoryServiceImpl implements CleaningHistoryService {
    @Autowired
    private CleaningHistoryRepository cleaningHistoryRepository;

    @Override
    public void saveCleaningHistory(CleaningHistory cleaningHistory) {
        cleaningHistoryRepository.save(cleaningHistory);
    }

    @Override
    public List<CleaningHistory> findAll() {
        return cleaningHistoryRepository.findAll();
    }

    @Override
    public Optional<CleaningHistory> findById(Date id) {
        return cleaningHistoryRepository.findById(id);
    }
}
