package com.seed.services;

import com.seed.models.CleaningHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CleaningHistoryService {
    void saveCleaningHistory(CleaningHistory cleaningHistory);
    List<CleaningHistory> findAll();
    Optional<CleaningHistory> findById(Date id);
}
