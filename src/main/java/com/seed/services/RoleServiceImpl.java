package com.seed.services;

import com.seed.repositories.RoleRepository;
import com.seed.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role save(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public Optional<Role> findById(Long id) {
		return roleRepository.findById(id);
	}
	@Override
	public Optional<Role> findByRole(String role) {
		return roleRepository.findByRole(role);
	}

	@Override
	public Iterable<Role> findAll() {
		return roleRepository.findAll();
	}
	@Override
	public void deleteById(Long id) {
		roleRepository.deleteById(id);
	}

	@Override
	public void delete(Role role) {
		roleRepository.delete(role);
	}
}
