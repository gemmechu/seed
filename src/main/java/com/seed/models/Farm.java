package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="farm",schema="seed_secure")
public class Farm {

	@Id
	private Long FarmID;

	@OneToMany(targetEntity = Variety.class)
	private Set<Variety> variety = new HashSet<>();

	public void setFarmVarietyHistories(Set<FarmVarietyHistory> farmVarietyHistories) {
		this.farmVarietyHistories = farmVarietyHistories;
	}

	public Set<FarmVarietyHistory> getFarmVarietyHistories() {
		return farmVarietyHistories;
	}

	@OneToMany(targetEntity = FarmVarietyHistory.class)
	private Set<FarmVarietyHistory> farmVarietyHistories = new HashSet<>();

	@ManyToOne(targetEntity = Branch.class)
	private Branch branch;

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	private String farmName;
	private String Region;

	public Long getFarmID() {
		return FarmID;
	}

	public void setFarmID(Long farmID) {
		FarmID = farmID;
	}

	public Set<Variety> getVariety() {
		return variety;
	}

	public void setVariety(Set<Variety> variety) {
		this.variety = variety;
	}

	public String getFarmName() {
		return farmName;
	}

	public void setFarmName(String farmName) {
		this.farmName = farmName;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}
}
