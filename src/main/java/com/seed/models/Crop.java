package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="crop",schema="seed_secure")
public class Crop {
	
	public Crop(String cropName) {
		this.cropName=cropName;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cropID;
	
	private String cropName;

	public Long getCropID() {
		return cropID;
	}

	public void setCropID(Long cropID) {
		this.cropID = cropID;
	}

	public String getCropName() {
		return cropName;
	}

	public void setCropName(String cropName) {
		this.cropName = cropName;
	}
//	@OneToMany(targetEntity = Variety.class)
//	private Set<Variety> variety = new HashSet<>();

}