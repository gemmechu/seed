package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Variety",schema="seed_secure")
public class Variety {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long varietyID;
	
	private String varietyName;
	@ManyToOne
	private Crop crop;
	
	private Date date;

	@PrePersist
	public void setDate() {
		this.date = new Date();
	}

	public Crop getCrop() {
		return crop;
	}

	public Long getVarietyID() {
		return varietyID;
	}
	public void setVarietyID(Long varietyID) {
		this.varietyID = varietyID;
	}
	public String getVarietyName() {
		return varietyName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}
}
