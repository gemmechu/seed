package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PSVHistory", schema = "seed_secure")
public class ProcessingStationVarietyHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;

    @OneToOne(targetEntity = Farm.class,fetch = FetchType.EAGER)
    private Farm farm = new Farm();
    @OneToOne(targetEntity = Variety.class)
    private Variety variety = new Variety();
    @ManyToOne(targetEntity = ProcessingStation.class)
    private ProcessingStation processingStation = new ProcessingStation();

    private int amount;


    private Status status;
    private Buyer buyer;

    private String buyerName;

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @PrePersist
    public void setDate() {
        this.date = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }


    public Variety getVariety() {
        return variety;
    }

    public void setVariety(Variety variety) {
        this.variety = variety;
    }

    public ProcessingStation getProcessingStation() {
        return processingStation;
    }

    public void setProcessingStation(ProcessingStation processingStation) {
        this.processingStation = processingStation;
    }

    public int getRecievedAmount() {
        return amount;
    }

    public void setRecievedAmount(int amount) {
        this.amount = amount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public enum Status {
        Dispatched, Recieved
    }

    public enum Buyer {
        FARMER, AGENT, INSTITUTIONALBUYER, DEALER
    }
}
