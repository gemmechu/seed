package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="processingStation",schema="seed_secure")
public class ProcessingStation {
	@Id
	private Long processingStationID;

	@OneToMany(targetEntity = Variety.class)
	private Set<Variety> variety = new HashSet<>();

	@ManyToOne(targetEntity = Branch.class)
	private Branch branch;
	private String processingStationName;
	@OneToMany(targetEntity = ProcessingStationVarietyHistory.class)
	private Set<ProcessingStationVarietyHistory>  processingStationVarietyHistory =new HashSet<>();
	@OneToMany(targetEntity = ProcessingStationVarietyStatus.class)
	private Set<ProcessingStationVarietyStatus> processingStationVarietyStatus= new HashSet<>();
	private String Region;




	public void setProcessingStationVarietyStatus(Set<ProcessingStationVarietyStatus> processingStationVarietyStatus) {
		this.processingStationVarietyStatus = processingStationVarietyStatus;
	}

	public void setProcessingStationVarietyHistory(Set<ProcessingStationVarietyHistory> processingStationVarietyHistory) {
		this.processingStationVarietyHistory = processingStationVarietyHistory;
	}

	public Set<ProcessingStationVarietyHistory> getProcessingStationVarietyHistory() {
		return processingStationVarietyHistory;
	}

	public Set<ProcessingStationVarietyStatus> getProcessingStationVarietyStatus() {
		return processingStationVarietyStatus;
	}

	public Long getProcessingStationID() {
		return processingStationID;
	}

	public void setProcessingStationID(Long processingStationID) {
		this.processingStationID = processingStationID;
	}

	public Set<Variety> getVariety() {
		return variety;
	}

	public void setVariety(Set<Variety> variety) {
		this.variety = variety;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getProcessingStationName() {
		return processingStationName;
	}

	public void setProcessingStationName(String processingStationName) {
		this.processingStationName = processingStationName;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}
}