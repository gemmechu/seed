package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="branch",schema="seed_secure")
public class Branch {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long branchID;
	
	private String branchName;

	private String Region;
	@OneToMany(targetEntity = ProcessingStation.class)
	private Set<ProcessingStation> processingStations = new HashSet<>();
	@OneToMany(targetEntity = Farm.class)
	private Set<Farm> farms = new HashSet<>();

	public Long getBranchID() {
		return branchID;
	}

	public void setBranchID(Long branchID) {
		this.branchID = branchID;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public Set<ProcessingStation> getProcessingStations() {
		return processingStations;
	}

	public void setProcessingStations(Set<ProcessingStation> processingStations) {
		this.processingStations = processingStations;
	}

	public Set<Farm> getFarms() {
		return farms;
	}

	public void setFarms(Set<Farm> farms) {
		this.farms = farms;
	}
}
