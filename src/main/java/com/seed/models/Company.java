package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="company",schema="seed_secure")
public class Company {

	@Id
	private Long companyID;
	
	private Date date;

	@PrePersist
	public void setDate() {
		this.date = new Date();
	}



	@OneToMany(targetEntity = Branch.class)
	private Set<Branch> Branchs = new HashSet<>();
	@OneToMany(targetEntity = ProcessingStation.class)
	private Set<ProcessingStation> processingStations = new HashSet<>();
	@OneToMany(targetEntity = Farm.class)
	private Set<Farm> farms = new HashSet<>();
	private String companyName;

	public Long getCompanyID() {
		return companyID;
	}

	public void setCompanyID(Long companyID) {
		this.companyID = companyID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setBranchs(Set<Branch> branchs) {
		Branchs = branchs;
	}

	public Set<ProcessingStation> getProcessingStations() {
		return processingStations;
	}

	public void setProcessingStations(Set<ProcessingStation> processingStations) {
		this.processingStations = processingStations;
	}

	public Set<Farm> getFarms() {
		return farms;
	}

	public void setFarms(Set<Farm> farms) {
		this.farms = farms;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Set<Branch> getBranchs(){
		return this.Branchs;
	}
	




}
