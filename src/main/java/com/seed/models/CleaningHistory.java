package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="CleaningHistory",schema="seed_secure")
public class CleaningHistory {
    @Id
    private Date cleaningHistoryId;

    public void setCleaningHistoryId(Date cleaningHistoryId) {
        this.cleaningHistoryId = cleaningHistoryId;
    }

    @ManyToOne(targetEntity = ProcessingStationVarietyStatus.class)
    private ProcessingStationVarietyStatus processingStationVarietyStatus;

    private int cleanedAmount;
    private int uncleanedAmount;
    private int wastedAmount;


    @PrePersist
    public void setDate(){
        cleaningHistoryId = new Date();
    }

    public Date getCleaningHistoryId() {
        return cleaningHistoryId;
    }

    public ProcessingStationVarietyStatus getProcessingStationVariety() {
        return processingStationVarietyStatus;
    }

    public void setProcessingStationVariety(ProcessingStationVarietyStatus processingStationVarietyStatus) {
        this.processingStationVarietyStatus = processingStationVarietyStatus;
    }

    public int getCleanedAmount() {
        return cleanedAmount;
    }

    public void setCleanedAmount(int cleanedAmount) {
        this.cleanedAmount = cleanedAmount;
    }

    public int getUncleanedAmount() {
        return uncleanedAmount;
    }

    public void setUncleanedAmount(int uncleanedAmount) {
        this.uncleanedAmount = uncleanedAmount;
    }

    public int getWastedAmount() {
        return wastedAmount;
    }

    public void setWastedAmount(int wastedAmount) {
        this.wastedAmount = wastedAmount;
    }
}
