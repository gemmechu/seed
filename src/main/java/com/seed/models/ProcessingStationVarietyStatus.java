package com.seed.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="ProcessingStationVarietyStatus",schema="seed_secure")
public class ProcessingStationVarietyStatus {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long varietyStatusID;
	
	private int cleaned;
	private int uncleaned;
	private int current;


	@ManyToOne(targetEntity = ProcessingStation.class)
    private ProcessingStation processingStation = new ProcessingStation(); 

    @OneToOne(targetEntity = Variety.class)
	private Variety variety = new Variety();


	public Long getVarietyStatusID() {
		return varietyStatusID;
	}

	public int getCleaned() {
		return cleaned;
	}
	public void setCleaned(int cleaned) {
		this.cleaned = cleaned;
	}
	public int getUncleaned() {
		return uncleaned;
	}
	public void setUncleaned(int uncleaned) {
		this.uncleaned = uncleaned;
	}

	public Variety getVariety() {
		return variety;
	}
	public void setVarieties(Variety variety) {
		this.variety = variety;
	}


	public void setCurrent(int current) {
		this.current = current;
	}

	public void setProcessingStation(ProcessingStation processingStation) {
		this.processingStation = processingStation;
	}

	public void setVariety(Variety variety) {
		this.variety = variety;
	}

	public int getCurrent() {
		return current;
	}

	public ProcessingStation getProcessingStation() {
		return processingStation;
	}
}
