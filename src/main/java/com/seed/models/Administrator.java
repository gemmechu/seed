package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Administrator",schema="seed_secure")
public class Administrator {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long administratorID;

	public Long getAdministratorID() {
		return administratorID;
	}

	public void setAdministratorID(Long administratorID) {
		this.administratorID = administratorID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAdministratorName() {
		return administratorName;
	}

	public void setAdministratorName(String administratorName) {
		this.administratorName = administratorName;
	}

	private Date date;

	@PrePersist
	public void setDate() {
		this.date = new Date();
	}
	
//	@OneToMany(targetEntity = Crop.class)
//	private Set<Crop> crop = new HashSet<>();
  
//	@OneToMany(targetEntity = Company.class)
//	private Set<Company> companies = new HashSet<>();
	
	private String administratorName;
	
}