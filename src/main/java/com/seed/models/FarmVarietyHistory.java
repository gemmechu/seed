package com.seed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="FarmVarietyHistory",schema="seed_secure")
public class FarmVarietyHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long farmVarietyHistoryID;

    private Date date;


    @ManyToOne(targetEntity = Farm.class)
    private Farm farm;
    @OneToOne(targetEntity = Variety.class)
    private Variety variety;
    @OneToOne(targetEntity = ProcessingStation.class)
    private ProcessingStation processingStation;

    private int recievedAmount;
    private int currentAmount;

    private Status status;
    public enum Status {
        RECEIVED, DISPATCHED
    }

    @PrePersist
    public void setDate() {
        this.date = new Date();
    }

    public Long getFarmVarietyHistoryID() {
        return farmVarietyHistoryID;
    }

    public void setFarmVarietyHistoryID(Long farmVarietyHistoryID) {
        this.farmVarietyHistoryID = farmVarietyHistoryID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public Variety getVariety() {
        return variety;
    }

    public void setVariety(Variety variety) {
        this.variety = variety;
    }

    public ProcessingStation getProcessingStation() {
        return processingStation;
    }

    public void setProcessingStation(ProcessingStation processingStation) {
        this.processingStation = processingStation;
    }

    public int getRecievedAmount() {
        return recievedAmount;
    }

    public void setRecievedAmount(int recievedAmount) {
        this.recievedAmount = recievedAmount;
    }

    public int getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(int currentAmount) {
        this.currentAmount = currentAmount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
