package com.seed.repositories;

import com.seed.models.CleaningHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface CleaningHistoryRepository extends JpaRepository<CleaningHistory,Date>{
}
