package com.seed.repositories;

import com.seed.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

	Role save(Role role);
	Optional<Role> findById(Long id);
	Optional<Role> findByRole(String role);
	List<Role> findAll();
	void deleteById(Long id);
	void delete(Role role);
}
