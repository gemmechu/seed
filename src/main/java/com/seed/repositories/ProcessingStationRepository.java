package com.seed.repositories;

import com.seed.models.ProcessingStation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProcessingStationRepository extends JpaRepository<ProcessingStation, Long>{
    Optional<ProcessingStation> findByProcessingStationName(String processingStationName);
}
