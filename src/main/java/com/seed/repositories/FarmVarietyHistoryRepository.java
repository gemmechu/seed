package com.seed.repositories;

import com.seed.models.Farm;
import com.seed.models.FarmVarietyHistory;
import com.seed.models.Variety;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FarmVarietyHistoryRepository extends JpaRepository<FarmVarietyHistory, Long>{
    List<FarmVarietyHistory> findByVarietyAndFarm(Variety variety, Farm farm);
}
