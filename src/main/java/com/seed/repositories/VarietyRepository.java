package com.seed.repositories;

import com.seed.models.Crop;
import com.seed.models.Variety;
import org.aspectj.weaver.ast.Var;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VarietyRepository extends JpaRepository<Variety, Long> {
    Optional<Variety> findByVarietyName(String varietyName);
    Optional<Variety> findByVarietyNameAndCrop(String varietyName, Crop crop);
}
