package com.seed.repositories;

import com.seed.models.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface BranchRepository extends JpaRepository<Branch, Long>{
    Optional<Branch> findByBranchName(String branchName);
}
