package com.seed.repositories;

import com.seed.models.ProcessingStationVarietyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessingStationVarietyHistoryRepository extends JpaRepository<ProcessingStationVarietyHistory,Long>{

}
