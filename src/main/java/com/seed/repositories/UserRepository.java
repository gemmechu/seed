package com.seed.repositories;

import com.seed.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	 
    User findByUsername(String username);
}