package com.seed.repositories;

import com.seed.models.Farm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface FarmRepository extends JpaRepository<Farm, Long> {

    Optional<Farm> findByFarmName(String farmName);

}
