package com.seed.repositories;

import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyStatus;
import com.seed.models.Variety;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ProcessingStationVarietyStatusRepository extends JpaRepository<ProcessingStationVarietyStatus, Long> {
    Optional<ProcessingStationVarietyStatus> findByVarietyAndProcessingStation(Variety variety, ProcessingStation processingStation);
    Set<ProcessingStationVarietyStatus> findByVariety(Variety variety);
}
