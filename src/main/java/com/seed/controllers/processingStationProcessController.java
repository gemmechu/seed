package com.seed.controllers;

import com.seed.models.*;
import com.seed.security.User;
import com.seed.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/processingStation/processingStationProcess")
public class processingStationProcessController {
    private CompanyService companyService;
    private UserService userService;
    private FarmService farmService;
    private VarietyService varietyService;
    private BranchService branchService;
    private ProcessingStationVarietyStatusService processingStationVarietyStatusService;
    private ProcessingStationVarietyHistoryService processingStationVarietyHistoryService;
    private ProcessingStationVarietyStatus processingStationVarietyStatus;
    private ProcessingStationService processingStationService;
    private CleaningHistoryService cleaningHistoryService;
    private static String varietyName;
    static Variety variety1;

    public processingStationProcessController() {

    }

    @Autowired
    public processingStationProcessController(UserService userService,ProcessingStationService processingStationService,BranchService branchService,VarietyService varietyService,FarmService farmService, CompanyService companyService,ProcessingStationVarietyHistoryService processingStationVarietyHistoryService,ProcessingStationVarietyStatusService processingStationVarietyStatusService, CleaningHistoryService cleaningHistoryService) {
        this.userService=userService;
        this.processingStationVarietyHistoryService=processingStationVarietyHistoryService;
        this.farmService=farmService;
        this.processingStationVarietyStatusService=processingStationVarietyStatusService;
        this.varietyService=varietyService;
        this.processingStationService=processingStationService;
        this.branchService=branchService;
        this.companyService=companyService;
        this.cleaningHistoryService = cleaningHistoryService;
    }
    @ModelAttribute(name="user")
    public User user() {
        return new User();
    }

    @ModelAttribute
    public void addVariety(Model model, @AuthenticationPrincipal UserDetails userDetails) {
    }

    @GetMapping
    public String getProcessing(@AuthenticationPrincipal UserDetails userDetails, @RequestParam long varietyID, Model model ) {
        Optional<Variety> variety= varietyService.findByVarietyID(varietyID);
        model.addAttribute("currentDate", new Date());
        if(variety.isPresent()) {
            variety1=variety.get();
            model.addAttribute("varieties",variety.get().getVarietyName());
            Optional<ProcessingStation> processingStation = processingStationService.findByProcessingStationID(userService.findUserByUsername(userDetails.getUsername()).getId());
            varietyName=variety.get().getVarietyName();
            if(processingStation.isPresent()){
                Optional<ProcessingStationVarietyStatus> processingStationVarietyStatus = processingStationVarietyStatusService.findByVarietyAndProcessingStation(variety1,processingStation.get());
                if (processingStationVarietyStatus.isPresent()){
                    model.addAttribute("currentAmount",processingStationVarietyStatus.get().getCurrent());
                }
            }
        }
        return "processingStationProcess";
    }

    @PostMapping
    public String processVariety(@AuthenticationPrincipal UserDetails userDetails, @RequestParam("cleaned") int cleanedAmount, @RequestParam("uncleaned") int uncleanedAmount, @RequestParam("wastage") int wastedAmount){
        CleaningHistory cleaningHistory = new CleaningHistory();
        cleaningHistory.setCleanedAmount(cleanedAmount);
        cleaningHistory.setUncleanedAmount(uncleanedAmount);
        cleaningHistory.setWastedAmount(wastedAmount);
        Optional<ProcessingStation> processingStationOptional = processingStationService.findByProcessingStationID(userService.findUserByUsername(userDetails.getUsername()).getId());
        Optional<Variety> varietyOptional = varietyService.findByVarietyName(varietyName);
        if(processingStationOptional.isPresent() && varietyOptional.isPresent()){
            Optional<ProcessingStationVarietyStatus> processingStationVarietyStatus = processingStationVarietyStatusService.findByVarietyAndProcessingStation(varietyOptional.get(),processingStationOptional.get());
            if(processingStationVarietyStatus.isPresent()){
                ProcessingStationVarietyStatus processingStationVariety1 = processingStationVarietyStatus.get();
                processingStationVariety1.setCurrent(processingStationVariety1.getCurrent()-cleaningHistory.getWastedAmount());
                processingStationVariety1.setCleaned(cleaningHistory.getCleanedAmount());
                processingStationVariety1.setUncleaned(cleaningHistory.getUncleanedAmount());
                cleaningHistory.setProcessingStationVariety(processingStationVariety1);
                updateProcessingStationVariety(processingStationVariety1);
                cleaningHistoryService.saveCleaningHistory(cleaningHistory);
                return "redirect:/processingStation";
            }
        }
        return "processingStationProcess";
    }
    @PatchMapping
    public void updateProcessingStationVariety(ProcessingStationVarietyStatus processingStationVarietyStatus){
        processingStationVarietyStatusService.saveProcessingStationVarietyStatus(processingStationVarietyStatus);
    }

}
