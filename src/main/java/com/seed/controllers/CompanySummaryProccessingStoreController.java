package com.seed.controllers;

import com.seed.models.Company;
import com.seed.models.ProcessingStation;
import com.seed.security.User;
import com.seed.services.CompanyService;
import com.seed.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/company/companySummary/summaryProccessingStore")
public class CompanySummaryProccessingStoreController {

    private CompanyService companyService;
    private UserService userService;
    public  CompanySummaryProccessingStoreController(){

    }
    @Autowired
    public  CompanySummaryProccessingStoreController(CompanyService companyService, UserService userService){
    this.companyService=companyService;
    this.userService=userService;
    }
    @GetMapping
    public String getSummary1() {
        return "summaryProccessingStore";
    }

    @ModelAttribute
    public  void addProcessingStore(@AuthenticationPrincipal UserDetails userDetails, Model model){
        User user =userService.findUserByUsername(userDetails.getUsername());
        Optional<Company> company=companyService.findByCompanyID(user.getId());
        if(company.isPresent()){
            Set<ProcessingStation> processingStations  =company.get().getProcessingStations();
            model.addAttribute("processingStations",processingStations);
        }
    }
}
