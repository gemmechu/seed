package com.seed.controllers;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import com.seed.models.Branch;
import com.seed.models.Farm;
import com.seed.services.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.seed.models.Company;
import com.seed.security.User;
import com.seed.services.BranchService;
import com.seed.services.CompanyService;
import com.seed.services.UserService;

@Controller
@RequestMapping("/company/add/addFarm")
public class AddFarmController {
    //Methods for adding stores
    private User currentUser;

    private UserService userService;
    private BranchService branchService;
    private FarmService farmService;
    private CompanyService companyService;

    public AddFarmController() {

    }

    @Autowired
    public AddFarmController(UserService userService, BranchService branchService, CompanyService companyService, FarmService farmService) {
        this.userService = userService;
        this.branchService = branchService;
        this.companyService = companyService;
        this.farmService = farmService;
    }

    @GetMapping
    public String addFarm(Model model, User user, @AuthenticationPrincipal UserDetails userDetails) {
        String username = userDetails.getUsername();
        user = userService.findUserByUsername(username);
        this.currentUser = user;
        model.addAttribute("currentUser", user);
        return "addFarm";
    }


    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

    @ModelAttribute
    public void addBranches(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<Company> company = companyService.findByCompanyID(user.getId());
        if (company.isPresent()) {
            Company company1 = company.get();
            if (company1.getBranchs().size() == 0 || company1.getBranchs() == null) {
                Set<Branch> brancheSet = new HashSet<>();
                Branch branch = new Branch();
                branch.setBranchName("No branches here");
                brancheSet.add(branch);
                model.addAttribute("branchs", brancheSet);
            } else {
                model.addAttribute("branchs", company1.getBranchs());
            }
        }

    }

    @PostMapping
    public String createFarm(@Valid User user, BindingResult bindingResult, @AuthenticationPrincipal UserDetails userDetails, Model model,
                             @RequestParam("name") String name, @RequestParam("region") String region, @RequestParam("branch") String branch) {
        User userExists = userService.findUserByUsername(user.getUsername());
        Optional<Branch> branchExists = branchService.findByBranchName(branch);
        Optional<Farm> farmExists = farmService.findByFarmName(name);
        if (userExists != null) {
            bindingResult.rejectValue("username", "error.username", "There is already a user registered with the username provided");
        }
        if(farmExists.isPresent()){
            model.addAttribute("farmMessage","There is already a farm registered with the name provided");
            return "addFarm";
        }
        if(!branchExists.isPresent() || branch.equalsIgnoreCase("No branches here")){
            model.addAttribute("branchMessage", "There is no branch registered with the given name.");
            return "addBranch";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("successMessage", "Registration has failed. Try Again with different values.");
            return "addFarm";
        }
        User user1 = userService.findUserByUsername(userDetails.getUsername());
        userService.saveFarm(user, "SEED_FARM", name, region, branch, user1.getId());
        model.addAttribute("successMessage", "User has been registered successfully");
        return "redirect:/company/add";
    }

}
