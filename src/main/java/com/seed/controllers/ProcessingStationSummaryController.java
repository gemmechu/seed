package com.seed.controllers;

import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyHistory;
import com.seed.models.ProcessingStationVarietyStatus;
import com.seed.security.User;
import com.seed.services.ProcessingStationService;
import com.seed.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/processingStation/processingStationSummary")
public class ProcessingStationSummaryController {

	private ProcessingStationService processingStationService;
	private UserService userService;
	public  ProcessingStationSummaryController(){

	}
	@Autowired
	public  ProcessingStationSummaryController(ProcessingStationService processingStationService, UserService userService){
		this.processingStationService=processingStationService;
		this.userService=userService;
	}

	@GetMapping
	public String getStoreSummary() {
		return "processingStationSummary";
	}

	@ModelAttribute
	public  void addProcessingStore(@AuthenticationPrincipal UserDetails userDetails, Model model){
		User user =userService.findUserByUsername(userDetails.getUsername());
		Optional<ProcessingStation> processingStation=processingStationService.findByProcessingStationID(user.getId());
		if(processingStation.isPresent()){
			Set<ProcessingStationVarietyStatus> processingStationVarietyStatuses  =processingStation.get().getProcessingStationVarietyStatus();
			Set<ProcessingStationVarietyHistory> varietyHistories  =processingStation.get().getProcessingStationVarietyHistory();
			model.addAttribute("processingStationVarietyStatuses",processingStationVarietyStatuses);
			model.addAttribute("varietyHistories",varietyHistories);
		}

	}
	@GetMapping("/all")
	public  String getSummaryAll(){
		return "processingStationHistorySummary";
	}
}
