package com.seed.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/farm")
public class FarmHomeController {
    @GetMapping
    public String getFarmHomePage(){
        return "farm";
    }
}
