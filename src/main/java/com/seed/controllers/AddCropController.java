package com.seed.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.seed.models.Crop;
import com.seed.security.User;
import com.seed.services.CropService;

@Controller
@RequestMapping("/administrator/addCrop")
public class AddCropController {
    private CropService cropService;

    @Autowired
    public AddCropController(CropService cropService) {
        this.cropService = cropService;
    }

    @GetMapping
    public String getAddCrop() {

        return "addCrop";
    }

    @ModelAttribute(name = "crop")
    public Crop crop(){
        return new Crop();
    }

    @PostMapping
    public String createFarm(@Valid Crop crop, BindingResult bindingResult, Model model) {
        Optional<Crop> crop2 = cropService.findByCropName(crop.getCropName());
        if (crop2.isPresent()) {
            bindingResult.rejectValue("cropName", "error.cropName", "There is already a crop registered with the name provided");
        }
        if (bindingResult.hasErrors()) {
            return "addCrop";
        }
        cropService.saveCrop(crop);
        return "redirect:/administrator";
    }

}
