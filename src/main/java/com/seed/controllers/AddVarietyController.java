package com.seed.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.seed.models.Crop;
import com.seed.models.Variety;
import com.seed.services.CropService;
import com.seed.services.VarietyService;

@Controller
@RequestMapping("/administrator/addVariety")
public class AddVarietyController {
    @Autowired
    CropService cropService;
    @Autowired
    VarietyService varietyService;
    static String cropName;

    @GetMapping
    public String getAddVariety(@RequestParam long cropID, Model model) {
        //System.out.println("path:"+cropID);
        Optional<Crop> crop = cropService.findByCropID(cropID);
        if (crop.isPresent()) {
            model.addAttribute("cropName", crop.get().getCropName());
            cropName = crop.get().getCropName();
        }
        System.out.println("path:" + cropID);
        return "addVariety";
    }

    @ModelAttribute(name = "variety")
    public Variety variety(){
        return new Variety();
    }

    @PostMapping
    public String addVariety(@Valid Variety variety, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addVariety";
        } else {
            Optional<Crop> crop = cropService.findByCropName(cropName);
            if (crop.isPresent()) {
                Optional<Variety> variety1 = varietyService.findByVarietyNameAndCrop(variety.getVarietyName(), crop.get());
                if(variety1.isPresent()){
                    bindingResult.rejectValue("varietyName", "error.varietyName", "There is already a variety registered with the name provided");
                }
                if (bindingResult.hasErrors()) {
                    return "addVariety";
                }
                variety.setCrop(crop.get());
                varietyService.saveVariety(variety);
            }
            return "redirect:/administrator";
        }
    }
}
