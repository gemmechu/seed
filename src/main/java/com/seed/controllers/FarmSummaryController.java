package com.seed.controllers;

import com.seed.models.Farm;
import com.seed.models.FarmVarietyHistory;
import com.seed.security.User;
import com.seed.services.FarmService;
import com.seed.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/farm/summary")
public class FarmSummaryController {
    private FarmService farmService;
    private UserService userService;
    public  FarmSummaryController(){

    }
    @Autowired
    public  FarmSummaryController(FarmService farmService, UserService userService){
        this.farmService=farmService;
        this.userService=userService;
    }

    @GetMapping
    public String getFarmSummary(){
        return "farmHistorySummary";
    }
    @ModelAttribute
    public  void addProcessingStore(@AuthenticationPrincipal UserDetails userDetails, Model model){
        User user =userService.findUserByUsername(userDetails.getUsername());
        Optional<Farm> farm=farmService.findByFarmID(user.getId());
        if(farm.isPresent()){
            Set<FarmVarietyHistory> farmVarietyHistories  =farm.get().getFarmVarietyHistories();
            model.addAttribute("farmVarietyHistories",farmVarietyHistories);

        }

    }

}
