package com.seed.controllers;

import com.seed.models.Branch;
import com.seed.models.Farm;
import com.seed.models.ProcessingStation;
import com.seed.security.User;
import com.seed.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/processingStation/processingStationRecieveFarm")
public class ProcessingStationRecieveFarmController {

    private CompanyService companyService;
    private UserService userService;
    private FarmService farmService;
    private BranchService branchService;

    private ProcessingStationService processingStationService;
    private static String farmName;

    public ProcessingStationRecieveFarmController() {

    }

    @Autowired
    public ProcessingStationRecieveFarmController(UserService userService, ProcessingStationService processingStationService, BranchService branchService, FarmService farmService, CompanyService companyService) {
        this.userService = userService;
        this.farmService = farmService;
        this.processingStationService = processingStationService;
        this.branchService = branchService;
        this.companyService = companyService;
    }

    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

    @GetMapping
    public String getRecieve() {

        return "processingStationRecieveFarms";
    }

    @ModelAttribute
    public void addFarms(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        Date date = new Date();
        model.addAttribute("date", date);
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<ProcessingStation> processingStation = processingStationService.findByProcessingStationID(user.getId());

        if (processingStation.isPresent()) {
            ProcessingStation processingStation1 = processingStation.get();
            Branch branch = processingStation1.getBranch();
            model.addAttribute("farms", branch.getFarms());
        }


    }

}
