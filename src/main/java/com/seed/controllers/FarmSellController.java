package com.seed.controllers;

import java.util.*;

import com.seed.models.*;
import com.seed.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.seed.security.User;

import javax.validation.Valid;

@Controller
@RequestMapping("/farm/sell")
public class FarmSellController {
    private CompanyService companyService;
    private UserService userService;
    private FarmService farmService;
    private BranchService branchService;
    private FarmVarietyHistoryService farmVarietyHistoryService;
    private VarietyService varietyService;
    private ProcessingStationService processingStationService;

    public FarmSellController() {

    }

    @Autowired
    public FarmSellController(UserService userService , BranchService branchService, FarmService farmService, CompanyService companyService, FarmVarietyHistoryService farmVarietyHistoryService, VarietyService varietyService, ProcessingStationService processingStationService) {
        this.userService = userService;
        this.farmService = farmService;
        this.branchService = branchService;
        this.companyService = companyService;
        this.farmVarietyHistoryService = farmVarietyHistoryService;
        this.varietyService = varietyService;
        this.processingStationService = processingStationService;
    }

    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

    private User currentUser;
    @GetMapping
    public String getSellPage(Model model, User user, @AuthenticationPrincipal UserDetails userDetails) {
        String username = userDetails.getUsername();
        user = userService.findUserByUsername(username);
        this.currentUser = user;
        model.addAttribute("currentUser", user);
        return "farmSell";
    }

    @PostMapping
    public String sellVariety(Model model, @AuthenticationPrincipal UserDetails userDetails,
                              @RequestParam(name = "amount") int amount,
                              @RequestParam(name = "processingStationName") String processingStationName,
                              @RequestParam(name = "varietyName") String varietyName) {
        FarmVarietyHistory farmVarietyHistory = new FarmVarietyHistory();
        String username = userDetails.getUsername();
        User user = userService.findUserByUsername(username);
        System.out.println(user.getUsername()+"\n" + user.getId());
        Optional<Farm> farm = farmService.findByFarmID(user.getId());
        if(farm.isPresent()) {
            farmVarietyHistory.setFarm(farm.get());
            Optional<ProcessingStation> processingStation = processingStationService.findByProcessingStationName(processingStationName);
            ProcessingStation processingStation1;
            if (processingStation.isPresent()) {
                farmVarietyHistory.setProcessingStation(processingStation.get());
                farmVarietyHistory.setRecievedAmount(amount);
                farmVarietyHistory.setStatus(FarmVarietyHistory.Status.DISPATCHED);
                farmVarietyHistory.setCurrentAmount(farmVarietyHistory.getCurrentAmount() - amount);
                Optional<Variety> variety1 = varietyService.findByVarietyName(varietyName);
                if (variety1.isPresent()) {
                    farmVarietyHistory.setVariety(variety1.get());
                    if (farmVarietyHistoryService.saveFarmVarietyHistoryService(farmVarietyHistory) == true) {
                        System.out.println("Successfully received.");
                        return "redirect:/farm";
                    } else {
                        System.out.println("Failed to receive");
                        return "farmSell";
                    }
                }
                return "farmSell";
            }
            return "farmSell";
        }
        return "farmSell";
    }
    @ModelAttribute
    public void addFarms(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<Farm> farm = farmService.findByFarmID(user.getId());
        if(farm.isPresent()){
            Farm farm1 = farm.get();
            Branch branch = farm1.getBranch();
            List<Variety> varieties = new ArrayList<>(farm1.getVariety());
            model.addAttribute("processingStations", branch.getProcessingStations());
            System.out.println("varieties in farm"+varieties.size());
            model.addAttribute("varieties",varieties);

        }
    }
    @ModelAttribute
    public void addVarieties(Model model) {
        Date date = new Date();
        model.addAttribute("date", date);
    }
}


/*
    @PostMapping
    public String receiveVariety(Model model, @AuthenticationPrincipal UserDetails userDetails, @RequestParam(name = "amount") int amount, @RequestParam(name="varietyName") String varietyName){
        FarmVarietyHistory farmVarietyHistory = new FarmVarietyHistory();
        String username = userDetails.getUsername();
        User user = userService.findUserByUsername(username);
        System.out.println(user.getUsername()+"\n" + user.getId());
        Optional<Farm> farm = farmService.findByFarmID(user.getId());
        if(farm.isPresent()){
            farmVarietyHistory.setFarm(farm.get());
            farmVarietyHistory.setProcessingStation(null);
            farmVarietyHistory.setRecievedAmount(amount);
            farmVarietyHistory.setStatus(FarmVarietyHistory.Status.RECEIVED);
            Optional<Variety> variety1 = varietyService.findByVarietyName(varietyName);
            if(variety1.isPresent()){
                farmVarietyHistory.setVariety(variety1.get());
                farmVarietyHistoryService.saveFarmVarietyHistoryService(farmVarietyHistory);
                return "redirect:/farm";
            }
            else{
                System.out.println("VARIETY IS NOT PRESENT");
                return "farmReceive";
            }
        }
        else{
            System.out.println("FARM IS NOT PRESENT");
            return "farmReceive";
        }

    }
}

 */
