package com.seed.controllers;

import javax.validation.Valid;

import com.seed.components.BranchValidator;
import com.seed.models.Branch;
import com.seed.services.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.seed.security.User;
import com.seed.services.UserService;

import java.util.Optional;

@Controller
@RequestMapping("/company/add/addBranch")
public class AddBranchController {


    @Autowired
    private UserService userService;
    @Autowired
    private BranchService branchService;

    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

    @GetMapping
    public String getAddBranch() {
        return "addBranch";
    }

    @PostMapping
    public String createNewBranch(@Valid User user, BindingResult bindingResult, @AuthenticationPrincipal UserDetails userDetails, Model model, @RequestParam("branchName") String name, @RequestParam("region") String region) {
        User userExists = userService.findUserByUsername(user.getUsername());
        Optional<Branch> branchExists = branchService.findByBranchName(name);
        if (userExists != null){
            bindingResult.rejectValue("username", "error.username", "There is already a user registered with the username provided");
        }
        if(branchExists.isPresent()){
            model.addAttribute("branchMessage", "There is already a branch registered with the name provided.");
            return "addBranch";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("successMessage", "Registration has failed. Try Again with different values.");
            return "addBranch";
        }
        User user1 = userService.findUserByUsername(userDetails.getUsername());
        userService.saveBranch(user, "SEED_BRANCH", name, region, user1.getId());
        model.addAttribute("successMessage", "User has been registered successfully");
        return "redirect:/company/add";
    }
}
