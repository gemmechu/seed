package com.seed.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.seed.security.User;
import com.seed.services.UserService;
  
@Controller
@RequestMapping("/administrator/register")
public class RegistrationController {

	@GetMapping
	public String index() {
		return "register"; 
	} 
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute(name="user")
	public User user() {
		return new User();
	}
	
	@PostMapping
	public String createNewUser(@Valid User user, BindingResult bindingResult, Model model, @RequestParam("name") String name) {
		User userExists = userService.findUserByUsername(user.getUsername());
	    if (userExists != null) {
	    	bindingResult.rejectValue("user", "error.user", "There is already a user registered with the username provided");
        }
		if (bindingResult.hasErrors()) {
		    return "registration";
		} 
		else {  
			userService.saveUser(user,"SEED_COMPANY", name);      
		    model.addAttribute("successMessage", "User has been registered successfully");  
		    return "redirect:/login"; // to return to login
		}
	}

}
