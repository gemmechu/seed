package com.seed.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seed.security.User;
import com.seed.services.UserService;

@Controller
@RequestMapping("/company")
public class CompanyHomeController {

	//Methods for root path of company
	@GetMapping
	public String index() {
		return "companyHome";
	}

	//Methods for company summary
	@GetMapping("/summary")
	public String summary() {
		return "companySummary";
	}

}
	