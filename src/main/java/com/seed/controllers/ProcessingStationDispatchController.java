package com.seed.controllers;

import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyHistory;
import com.seed.models.Variety;
import com.seed.security.User;
import com.seed.services.ProcessingStationService;
import com.seed.services.ProcessingStationVarietyHistoryService;
import com.seed.services.UserService;
import com.seed.services.VarietyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/processingStation/processingStationDispatchForm")
public class ProcessingStationDispatchController {
	
	private VarietyService varietyService;
	private UserService userService;
	private ProcessingStationService processingStationService;
	private ProcessingStationVarietyHistoryService processingStationVarietyHistoryService;

	Variety variety1;
	@Autowired
	public ProcessingStationDispatchController(VarietyService varietyService, UserService userService, ProcessingStationVarietyHistoryService processingStationVarietyHistoryService, ProcessingStationService processingStationService){
		this.varietyService=varietyService;
		this.processingStationVarietyHistoryService = processingStationVarietyHistoryService;
		this.userService = userService;
		this.processingStationService = processingStationService;
	}
	@GetMapping
	public String getDispatch(@RequestParam long varietyID, Model model ) {

		Optional<Variety> variety= varietyService.findByVarietyID(varietyID);
		if(variety.isPresent()) {
			variety1=variety.get();
			Date date= new Date();
			model.addAttribute("variety",variety.get());
			model.addAttribute("date",date);


		}

		return "processingStationDispatchForm";
	}
	@PostMapping
	public String dispatchOrder(@AuthenticationPrincipal UserDetails userDetails,
								@RequestParam("buyer") String buyerType,
								@RequestParam("buyerName") String buyerName,
								@RequestParam("amount") int amount){
        ProcessingStationVarietyHistory processingStationVarietyHistory = new ProcessingStationVarietyHistory();
		User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<ProcessingStation> processingStationOptional = processingStationService.findByProcessingStationID(user.getId());
        if(processingStationOptional.isPresent()){
            ProcessingStation processingStation = processingStationOptional.get();
            processingStationVarietyHistory.setAmount(amount);
            processingStationVarietyHistory.setFarm(null);
            processingStationVarietyHistory.setProcessingStation(processingStation);
            processingStationVarietyHistory.setVariety(variety1);
            if(buyerType.equalsIgnoreCase(ProcessingStationVarietyHistory.Buyer.AGENT.name())){
                processingStationVarietyHistory.setBuyer(ProcessingStationVarietyHistory.Buyer.AGENT);
            }
            else if(buyerType.equalsIgnoreCase(ProcessingStationVarietyHistory.Buyer.DEALER.name())){
                processingStationVarietyHistory.setBuyer(ProcessingStationVarietyHistory.Buyer.DEALER);

            }
            else if(buyerType.equalsIgnoreCase(ProcessingStationVarietyHistory.Buyer.FARMER.name())){
                processingStationVarietyHistory.setBuyer(ProcessingStationVarietyHistory.Buyer.FARMER);
            }
            else if(buyerType.equalsIgnoreCase(ProcessingStationVarietyHistory.Buyer.INSTITUTIONALBUYER.name())){
                processingStationVarietyHistory.setBuyer(ProcessingStationVarietyHistory.Buyer.INSTITUTIONALBUYER);
            }
            processingStationVarietyHistory.setBuyerName(buyerName);
            processingStationVarietyHistory.setStatus(ProcessingStationVarietyHistory.Status.Dispatched);
            processingStationVarietyHistoryService.save(processingStationVarietyHistory);
            return "redirect:/processingStation";
        }
		return "processingStationDispatchForm";
	}
}
