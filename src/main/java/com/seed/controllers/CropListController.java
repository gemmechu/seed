package com.seed.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seed.models.Crop;
import com.seed.services.CropService;
@Controller
@RequestMapping("administrator/CropList")
public class CropListController {
	@Autowired
	CropService cropService;
	
	@GetMapping
	public String getCropList() {
		return "CropList";
	}
	
	@ModelAttribute
	public void addCrops(Model model) {
		List<Crop> crops = cropService.findAll();
		model.addAttribute("crops", crops);
	}

}
