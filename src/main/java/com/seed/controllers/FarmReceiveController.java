package com.seed.controllers;

import java.util.*;

import com.seed.components.FarmHistoryValidator;
import com.seed.models.*;
import com.seed.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.seed.security.User;

import javax.persistence.Convert;
import javax.validation.Valid;

@Controller
@RequestMapping("/farm/receive")
public class FarmReceiveController {
    private CompanyService companyService;
    private UserService userService;
    private FarmService farmService;
    private BranchService branchService;
    private FarmVarietyHistoryService farmVarietyHistoryService;
    private VarietyService varietyService;

    public FarmReceiveController() {

    }

    @Autowired
    public FarmReceiveController(UserService userService, ProcessingStationService processingStationService, BranchService branchService, FarmService farmService, CompanyService companyService, FarmVarietyHistoryService farmVarietyHistoryService, VarietyService varietyService) {
        this.userService = userService;
        this.farmService = farmService;
        this.branchService = branchService;
        this.companyService = companyService;
        this.farmVarietyHistoryService = farmVarietyHistoryService;
        this.varietyService = varietyService;
    }
    private User currentUser;

    @GetMapping
    public String getReceivePage(Model model, User user, @AuthenticationPrincipal UserDetails userDetails) {
        String username = userDetails.getUsername();
        user = userService.findUserByUsername(username);
        this.currentUser = user;
        model.addAttribute("currentUser", user);
        return "farmReceive";
    }

    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }
    @ModelAttribute(name = "farmVarietyHistory")
    public FarmVarietyHistory farmVarietyHistory() {
        return new FarmVarietyHistory();
    }

    @Autowired
    FarmHistoryValidator farmHistoryValidator;

    @PostMapping
    public String receiveVariety(@ModelAttribute("farmVarietyHistory") FarmVarietyHistory farmVarietyHistory, BindingResult bindingResult, @AuthenticationPrincipal UserDetails userDetails, @RequestParam(name = "amount") String amount, @RequestParam(name="varietyName") String varietyName){
        String username = userDetails.getUsername();
        User user = userService.findUserByUsername(username);
        System.out.println(user.getUsername()+"\n" + user.getId());
        Optional<Farm> farm = farmService.findByFarmID(user.getId());
        if(farm.isPresent()){
            farmVarietyHistory.setFarm(farm.get());
            farmVarietyHistory.setProcessingStation(null);
            try {
                farmVarietyHistory.setRecievedAmount(Integer.parseInt(amount));
            }
            catch (Exception ex){
                bindingResult.rejectValue("recievedAmount", "error.recievedAmount", "Received Amount should be a valid input.");
            }
            farmVarietyHistory.setStatus(FarmVarietyHistory.Status.RECEIVED);
            Optional<Variety> variety1 = varietyService.findByVarietyName(varietyName);
            if(variety1.isPresent()){
                farmVarietyHistory.setVariety(variety1.get());
                farmHistoryValidator.validate(farmVarietyHistory, bindingResult);
                if(bindingResult.hasErrors()){
                    return "farmReceive";
                }

                if(farmVarietyHistoryService.saveFarmVarietyHistoryService(farmVarietyHistory)==true){
                    System.out.println("Successfully received");
                    return "redirect:/farm";
                }
                else{
                    System.out.println("Failed to received");
                    return "farmReceive";
                }
            }
            else{
                System.out.println("VARIETY IS NOT PRESENT");
                return "farmReceive";
            }
        }
        else{
            System.out.println("FARM IS NOT PRESENT");
            return "farmReceive";
        }

    }

    @ModelAttribute
    public void addVarieties(Model model) {
        Date date = new Date();
        model.addAttribute("date", date);
        model.addAttribute("varieties",varietyService.findAll());
    }
}
