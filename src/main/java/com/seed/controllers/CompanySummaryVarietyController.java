package com.seed.controllers;

import com.seed.models.Company;
import com.seed.models.ProcessingStation;
import com.seed.models.ProcessingStationVarietyStatus;
import com.seed.models.Variety;
import com.seed.security.User;
import com.seed.services.CompanyService;
import com.seed.services.ProcessingStationVarietyStatusService;
import com.seed.services.UserService;
import com.seed.services.VarietyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RequestMapping("/company/companySummary/summaryVariety")
public class CompanySummaryVarietyController {
    private CompanyService companyService;
    private UserService userService;
    private ProcessingStationVarietyStatusService processingStationVarietyStatusService;
    public CompanySummaryVarietyController(){

    }
    @Autowired
    public CompanySummaryVarietyController(CompanyService companyService, UserService userService, ProcessingStationVarietyStatusService processingStationVarietyStatusService){
    this.companyService=companyService;
    this.processingStationVarietyStatusService=processingStationVarietyStatusService;
    this.userService=userService;
    }
    @GetMapping
    public String getSummary1() {
        return "summaryVariety";
    }

    @Autowired
    private VarietyService varietyService;
    @ModelAttribute
    public  void addProcessingStore(@AuthenticationPrincipal UserDetails userDetails, Model model){
        User user =userService.findUserByUsername(userDetails.getUsername());
        Optional<Company> company=companyService.findByCompanyID(user.getId());
//        List<Variety> varietyList = new ArrayList<>();
//        if(company.isPresent()){
//           Set<ProcessingStation> processingStations  =company.get().getProcessingStations();
//            for (ProcessingStation processingStation:processingStations
//                 ) {
//                Set<ProcessingStationVarietyStatus> processingStationVarietyStatus = processingStation.getProcessingStationVarietyStatus();
//                for (ProcessingStationVarietyStatus processingStationVarietyStatus1: processingStationVarietyStatus
//                     ) {
//                    Variety variety = processingStationVarietyStatus1.getVariety();
//                    if(!varietyList.contains(variety)){
//                        varietyList.add(variety);
//                    }
//                }
//
//                }
//            model.addAttribute("processingStations",processingStations);
           // Set<Variety> varieties=company.get().get
          //  Optional<Set<ProcessingStationVarietyStatus>> processingStationVarietyStatuses=processingStationVarietyStatusService.findByVariety();
        }
    }

