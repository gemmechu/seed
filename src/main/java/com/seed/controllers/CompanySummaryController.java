package com.seed.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/company/companySummary")
public class CompanySummaryController {

	@GetMapping
	public String getSummary() {
		return "companySummary";
	}
	

//	@GetMapping("summaryVariety")
//	public String getSummary2() {
//		return "summaryVariety";
//	}
	@GetMapping("/summaryFarmStore")
	public String getSummary3() {
		return "summaryFarmStore";
	}
}
