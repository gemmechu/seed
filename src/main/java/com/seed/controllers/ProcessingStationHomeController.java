package com.seed.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/processingStation")
public class ProcessingStationHomeController {

	@GetMapping
	public String index() {
		return "processingStation";
	}
}
