package com.seed.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.seed.models.*;
import com.seed.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.seed.security.User;

@Controller
@RequestMapping("/processingStation/processingStationRecieve")
public class ProcessingStationRecieveController {
    private CompanyService companyService;
    private UserService userService;
    private FarmService farmService;
    private VarietyService varietyService;
    private BranchService branchService;
    private ProcessingStationVarietyStatusService processingStationVarietyService;
    private ProcessingStationVarietyHistoryService processingStationVarietyHistoryService;
    private ProcessingStationVarietyStatusService processingStationVarietyStatusService;
    private ProcessingStationService processingStationService;
    private static String farmName;
    static Farm farm1;

    public ProcessingStationRecieveController() {

    }

    @Autowired
    public ProcessingStationRecieveController(UserService userService,ProcessingStationService processingStationService,BranchService branchService,VarietyService varietyService,FarmService farmService, CompanyService companyService,ProcessingStationVarietyHistoryService processingStationVarietyHistoryService,ProcessingStationVarietyStatusService processingStationVarietyStatusService) {
        this.userService=userService;
        this.processingStationVarietyHistoryService=processingStationVarietyHistoryService;
        this.farmService=farmService;
        this.processingStationVarietyStatusService = processingStationVarietyStatusService;
        this.varietyService=varietyService;
        this.processingStationService=processingStationService;
        this.branchService=branchService;
        this.companyService=companyService;
    }
    @ModelAttribute(name="user")
    public User user() {
        return new User();
    }

    @ModelAttribute
    public void addFarms(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        Date date = new Date();
        model.addAttribute("date", date);
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<ProcessingStation> processingStation = processingStationService.findByProcessingStationID(user.getId());

        if (processingStation.isPresent()) {
            ProcessingStation processingStation1 = processingStation.get();
            Branch branch = processingStation1.getBranch();
            model.addAttribute("farms", branch.getFarms());
        }


    }
    @GetMapping
    public String getRecieving(@RequestParam long farmID, Model model ) {

        Optional<Farm> farm= farmService.findByFarmID(farmID);
        if(farm.isPresent()) {
            farm1=farm.get();
            model.addAttribute("farmName",farm.get().getFarmName());
            farmName=farm.get().getFarmName();
            model.addAttribute("varieties",farm.get().getVariety());
        }

        return "processingStationRecieve";
    }
    @PostMapping
    public String recieveVariety(@AuthenticationPrincipal UserDetails userDetails, @RequestParam(name="variety") String varietyName,@RequestParam(name="amount") int amount
    ){  ProcessingStationVarietyHistory processingStationVarietyHistory =new ProcessingStationVarietyHistory();
        User user=userService.findUserByUsername(userDetails.getUsername());
        Optional<ProcessingStation> processingStation =processingStationService.findByProcessingStationID(user.getId());
        Optional<Variety> variety =varietyService.findByVarietyName(varietyName);
        if(processingStation.isPresent()){
            if (variety.isPresent()){
                processingStationVarietyHistory.setVariety(variety.get());
                processingStationVarietyHistory.setProcessingStation(processingStation.get());
                processingStationVarietyHistory.setFarm(farm1);
                processingStationVarietyHistory.setAmount(amount);
                processingStationVarietyHistory.setStatus(ProcessingStationVarietyHistory.Status.Dispatched);
                processingStationVarietyHistoryService.save(processingStationVarietyHistory);
                Set<ProcessingStationVarietyHistory> processingStationVarietyHistories = processingStation.get().getProcessingStationVarietyHistory();
                processingStationVarietyHistories.add(processingStationVarietyHistory);
                processingStation.get().setProcessingStationVarietyHistory(processingStationVarietyHistories);
                Optional<ProcessingStationVarietyStatus> processingStationVarietyStatus= processingStationVarietyStatusService.findByVarietyAndProcessingStation(variety.get(),processingStation.get());
                if(processingStationVarietyStatus.isPresent()){

                    processingStationVarietyStatus.get().setUncleaned(processingStationVarietyStatus.get().getUncleaned()+amount);
                    processingStationVarietyStatus.get().setCurrent(processingStationVarietyStatus.get().getCurrent() +amount);
                    updateProcessingStationVariety( processingStationVarietyStatus.get());
                    Set<ProcessingStationVarietyStatus> processingStationVarietyStatusSet = processingStation.get().getProcessingStationVarietyStatus();
                    processingStationVarietyStatusSet.add( processingStationVarietyStatus.get());
                    processingStation.get().setProcessingStationVarietyStatus(processingStationVarietyStatusSet);
                    processingStationService.saveProcessingStation( processingStation.get());


                }
                else{
                    System.out.println("ok guud");
                    ProcessingStationVarietyStatus processingStationVariety1 =new ProcessingStationVarietyStatus();
                    processingStationVariety1.setUncleaned(amount);
                    processingStationVariety1.setVarieties(variety.get());
                    processingStationVariety1.setCurrent(amount);
                    processingStationVariety1.setProcessingStation(processingStation.get());
                    processingStationVariety1.setCleaned(0);
                    processingStationVarietyStatusService.saveProcessingStationVarietyStatus(processingStationVariety1);
                    Set<ProcessingStationVarietyStatus> processingStationVarietyStatusSet = processingStation.get().getProcessingStationVarietyStatus();
                    processingStationVarietyStatusSet.add(processingStationVariety1);
                    processingStation.get().setProcessingStationVarietyStatus(processingStationVarietyStatusSet);
                    processingStationService.saveProcessingStation( processingStation.get());
                }
            }


        }
        return "processingStationRecieve";
    }
    @PatchMapping
    public void updateProcessingStationVariety(ProcessingStationVarietyStatus processingStationVarietyStatus){
        processingStationVarietyStatusService.saveProcessingStationVarietyStatus(processingStationVarietyStatus);
    }
}
