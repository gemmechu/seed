package com.seed.controllers;

import com.seed.models.Branch;
import com.seed.models.Company;
import com.seed.models.Farm;
import com.seed.models.ProcessingStation;
import com.seed.security.User;
import com.seed.services.BranchService;
import com.seed.services.CompanyService;
import com.seed.services.ProcessingStationService;
import com.seed.services.UserService;

import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/company/add/addProcessingStation")

public class AddProcessingStationController {

    //Methods for adding stores
    private User currentUser;

    private UserService userService;
    private BranchService branchService;
    private ProcessingStationService processingStationService;
    private CompanyService companyService;

    public AddProcessingStationController() {

    }

    @Autowired
    public AddProcessingStationController(UserService userService, BranchService branchService, CompanyService companyService, ProcessingStationService processingStationService) {
        this.userService = userService;
        this.branchService = branchService;
        this.companyService = companyService;
        this.processingStationService = processingStationService;
    }

    @GetMapping
    public String addStore(Model model, User user, @AuthenticationPrincipal UserDetails userDetails) {
        String username = userDetails.getUsername();
        user = userService.findUserByUsername(username);
        this.currentUser = user;
        model.addAttribute("currentUser", user);
        return "addProcessingStation";
    }


    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

    @ModelAttribute
    public void addBranches(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<Company> company = companyService.findByCompanyID(user.getId());
        if (company.isPresent()) {
            Company company1 = company.get();
            if(company1.getBranchs().size()==0 || company1.getBranchs()==null){
                Set<Branch> brancheSet = new HashSet<>();
                Branch branch = new Branch();
                branch.setBranchName("No branches here");
                brancheSet.add(branch);
                model.addAttribute("branchs",brancheSet);
            }
            else{
                model.addAttribute("branchs", company1.getBranchs());
            }
        }


    }

    @PostMapping
    public String createNewUser(@Valid User user, BindingResult bindingResult, @AuthenticationPrincipal UserDetails userDetails, Model model,
                                @RequestParam("name") String name, @RequestParam("region") String region, @RequestParam("branch") String branch) {
        User userExists = userService.findUserByUsername(user.getUsername());
        Optional<Branch> branchExists = branchService.findByBranchName(branch);
        Optional<ProcessingStation> processingStationExists = processingStationService.findByProcessingStationName(name);
        if (userExists != null) {
            bindingResult.rejectValue("username", "error.username", "There is already a user registered with the username provided");
        }
        if(processingStationExists.isPresent()){
            model.addAttribute("processingStationMessage","There is already a processing station registered with the name provided");
            return "addProcessingStation";
        }
        if(!branchExists.isPresent() || branch.equalsIgnoreCase("No branches here")){
            model.addAttribute("branchMessage", "There is no branch registered with the given name.");
            return "addProcessingStation";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("successMessage", "Registration has failed. Try Again with different values.");
            return "addProcessingStation";
        }
        User user1 = userService.findUserByUsername(userDetails.getUsername());
        userService.saveProcessingStation(user, "SEED_PROCESSING_STATION", name, region, branch, user1.getId());
        model.addAttribute("successMessage", "User has been registered successfully");
        return "redirect:/company/add";
    }
}
