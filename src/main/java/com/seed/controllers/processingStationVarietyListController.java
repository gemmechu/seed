package com.seed.controllers;

import com.seed.models.ProcessingStation;
import com.seed.models.Variety;
import com.seed.security.User;
import com.seed.services.ProcessingStationService;
import com.seed.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/processingStation/processingStationVarietyList")
public class processingStationVarietyListController {

    private ProcessingStationService processingStationService;
    private UserService userService;
    public processingStationVarietyListController(){}

    @Autowired
    public processingStationVarietyListController(ProcessingStationService processingStationService,UserService userService){
        this.processingStationService=processingStationService;
        this.userService=userService;
    }

    @ModelAttribute
    public void getVarieties(@AuthenticationPrincipal UserDetails userDetails){

    }

    @GetMapping
    public  String getVarietyList(@AuthenticationPrincipal UserDetails userDetails,Model model ){
        User user = userService.findUserByUsername(userDetails.getUsername());
        Optional<ProcessingStation> processingStation=processingStationService.findByProcessingStationID(user.getId());
        if(processingStation.isPresent()){
            Set<Variety> varieties= processingStation.get().getVariety();
            model.addAttribute("varieties",varieties);
        }
        return "processingStationVarietyList";

    }

}
