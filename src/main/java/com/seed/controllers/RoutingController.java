package com.seed.controllers;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RoutingController {
	@RequestMapping("/default")
	public String routeLogin(@AuthenticationPrincipal UserDetails userDetails) {
		if(userDetails.getAuthorities().toString().equals("[SEED_PROCESSING_STATION]")) {
			return "redirect:/processingStation";
		}
		else if(userDetails.getAuthorities().toString().equals("[SEED_BRANCH]")) {
			return "redirect:/branch";
		}
		else if(userDetails.getAuthorities().toString().equals("[SEED_FARM]")) {
			return "redirect:/farm";
		}
		else if(userDetails.getAuthorities().toString().equals("[SEED_COMPANY]")) {
			return "redirect:/company";
		}
		else if(userDetails.getAuthorities().toString().equals("[SEED_ADMINISTRATOR]")) {
			return "redirect:/administrator";
		}
		return "redirect:/";
	}
}
