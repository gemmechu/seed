package com.seed.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seed.security.User;
import com.seed.services.UserService;


@Controller
@RequestMapping("/company/add")
public class AddController {
    @Autowired
    private UserService userService;
    private User currentUser;

    @GetMapping
    public String getAdd() {

        return "add";
    }

    @ModelAttribute(name = "user")
    public User user() {
        return new User();
    }

}
