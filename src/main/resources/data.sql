insert into role(id,role) values(1,'SEED_ADMINISTRATOR');
insert into role(id,role) values(2,'SEED_COMPANY');
insert into role(id,role) values(3,'SEED_PROCESSING_STATION');
insert into role(id,role) values(4,'SEED_BRANCH');
insert into role(id,role) values(5,'SEED_FARM');
insert into user(id,enabled,password,username) values (1, 1, '$2a$10$vxvm7bpIxe6dvpVUixcrN.Eyw/IbCAJNo6Q.Q1Rf2X.JKndpUrzJ.', 'administratorUserName');
insert into user_role(user_id,role_id) values (1,1);
